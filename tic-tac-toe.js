const gameField = [
    ['x', 'x', 'o'],
    ['o', 'o', 'x'],
    ['x', 'o', null],
  ];
function game(area) {
    for (let i = 0; i < area.length; i++) {
        let firstElement = area[i][0] ?? '';
        let res = true;
        for (let j = 0; j < area[i].length; j++) {
            if (area[i][j] !== firstElement) {
                res = false;
                break;
            }
        }
        if (res) {
            return `Победил "${firstElement}" по горизонтали`;
        }
        firstElement = area[0][i] ?? '';
        res = true;
        for (let j = 0; j < area.length; j++) {
            if (area[j][i] !== firstElement) {
                res = false;
                break;
            }
        }
        if (res) {
            return `Победили "${firstElement}" по вертикали`;
        }
    }
    let len = area.length - 1;
    const mainDiag = [];
    const secDiag = [];
    for (let i = 0; i < area.length; i++) {
        mainDiag.push(area[i][i]);
        secDiag.push(area[i][len - i]);
    }
    let firstElement = mainDiag[0] ?? '';
    let res = true;
    for (let i = 0; i < mainDiag.length; i++) {
        if (mainDiag[i] !== firstElement) {
            res = false;
            break;
        }
    }
    if (res) {
        return `Победил "${firstElement}" по диагонали`;
    }
    firstElement = secDiag[0] ?? '';
    res = true;
    for (let i = 0; i < secDiag.length; i++) {
        if (secDiag[i] !== firstElement) {
            res = false;
            break;
        }
    }
    if (res) {
        return `Победил "${firstElement}" по диагонали`;
    }
    return "Ничья";
}
console.log(game(gameField));