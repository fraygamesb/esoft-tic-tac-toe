class gameField {
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];
    gameResult = '';
    mode = 'x';
    isGameOver = false;
    setMode() {
        this.mode = (this.mode === 'x') ? 'o' : 'x';
    }
    fieldCellValue(coordinates) {
        if (this.state[coordinates.x - 1][coordinates.y - 1] === null) {
            this.state[coordinates.x - 1][coordinates.y - 1] = this.mode;
            return true;
        } else {
            return false;
        }
    }
    getGameFieldStatus() {
        const area = this.state;
        for (let i = 0; i < area.length; i++) {
            let firstElement = area[i][0] ?? '';
            let res = true;
            for (let j = 0; j < area[i].length; j++) {
                if (area[i][j] !== firstElement) {
                    res = false;
                    break;
                }
            }
            if (res) {
                this.isGameOver = true;
                return `Победил "${firstElement}" по горизонтали`;
            }
            firstElement = area[0][i] ?? '';
            res = true;
            for (let j = 0; j < area.length; j++) {
                if (area[j][i] !== firstElement) {
                    res = false;
                    break;
                }
            }
            if (res) {
                this.isGameOver = true;
                return `Победили "${firstElement}" по вертикали`;
            }
        }
        let len = area.length - 1;
        const mainDiag = [];
        const secDiag = [];
        for (let i = 0; i < area.length; i++) {
            mainDiag.push(area[i][i]);
            secDiag.push(area[i][len - i]);
        }
        let firstElement = mainDiag[0] ?? '';
        let res = true;
        for (let i = 0; i < mainDiag.length; i++) {
            if (mainDiag[i] !== firstElement) {
                res = false;
                break;
            }
        }
        if (res) {
            this.isGameOver = true;
            return `Победил "${firstElement}" по диагонали`;
        }
        firstElement = secDiag[0] ?? '';
        res = true;
        for (let i = 0; i < secDiag.length; i++) {
            if (secDiag[i] !== firstElement) {
                res = false;
                break;
            }
        }
        const area1D = [].concat(...area);
        console.log(area1D)
        if(area1D.find(item => item === null) === undefined) {
            this.isGameOver = true;
            this.gameResult = 'Ничья';
        }
    }
}

gf = new gameField()
while (!gf.isGameOver) {
    alert(`Ходит ${gf.mode}`)
    const x = prompt('Введите номер строки')
    const y = prompt('Введите номер столбца')
    const coordinates = {
        x: x,
        y: y,
    }
    if (!gf.fieldCellValue(coordinates)) {
        console.log('Эта клетка уже заполнена!');
    } else {
        gf.getGameFieldStatus()
        gf.setMode()
    }
}

console.log(gf.gameResult);
